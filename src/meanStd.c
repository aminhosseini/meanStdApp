/ ============================================================================
//  Copyright (c) 2019 - 2022, European Spallation Source ERIC
// 
//  The program is free software: you can redistribute it and/or modify it
//  under the terms of the BSD 3-Clause license.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.
//  
//  Author  : Amin Hosseini Nejad
//  email   : amin.hosseininejad@ess.eu
//  Date    : 2022-08-30
//  version : 0.0.0
//  
// ============================================================================ 
//
// The c++ subroutine takes three arrays, one of number of seconds passed EPICS epoch refered as array "seconds",
// another of number of nanoseconds within each second as array "nano_seconds",
// and another of VAL values of PVs as array "val" as inputs 
// and calculate mean and standard deviation values withing desired time intervals, and output them to the meanStd
// template using its aSub record



#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdexcept.h>
#include <math.h>


static int meanStd(aSubRecord* prec) {

    int* seconds = (int*)prec->a; // pointer to the array "seconds"
    int* nano_seconds = (int*)prec->b; // pointer to the array "nano_seconds"
    double* val = (double*)prec->c; // pointer to the array "val"
    unsigned nElements = prec->nea;   // nr of elements in each arrays

    double* mean = (double*)prec->vala;
    double* std = (double*)prec->valb;

    int sum_delta_time_mean = 0;
    int sum_delta_time_std = 0;
    int sum_val_time = 0;
    double sum_val_mean_diff_squared_time = 0;
    int delta_time;

    int i, j = 0, 0;

    //adding the seconds and nano-seconds arrays together
    double* time;
    for (int k=0 ; k < (nElements) ; k++) 
        time[k] = seconds[k] + (double)nano_sconds[k] / 1e9;

    do {

        delta_time = time[i + 1] - time[i];
        double val_time = val[i] * delta_time;

        sum_delta_time_mean += delta_time;
        sum_val_time += val_time;

        i++;

    } while (sum_delta_time < nElements);

    mean[0] = (double)sum_val_by_time / sum_delta_time;
    
    do {
        delta_time = time[j + 1] - time[j];
        double val_mean_diff_squared = (val[j] - mean) ** 2;
        double val_mean_diff_squared_time = val_mean_diff_squared * delta_time;

        sum_delta_time_std += delta_time;
        sum_val_mean_diff_squared_time += val_mean_diff_squared_time;

        j++;

    } while (sum_delta_time_std < nElements);

    std[0] = sqrt((double)sum_val_mean_diff_squared_time / sum_delta_time_std);
    
    printf("[mean, std] : %f\t%f\n", mean[0], std[0]);


        return 0; 
}
epicsRegisterFunction(meanStd);

//# - end of file

